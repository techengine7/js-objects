var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
/*	Создать массив students. Заполнить его данными из studentsAndPoints. Каждый элемент массива это объект в поле name имя студента а в поле point его балл. 
	Так же добавить метод show который выводит в консоль строку вида: Студент Виктория Заровская набрал 30 баллов 
	*/
	
var students = [];//Массив объектов-студентов

//Комментарий преподавателя 1. У студентов нет метода show 2. Вы создали функцию addStudent но полностью воспроизвели её код в первом задании
//**********ДОРАБОТКА	
	function addStudent(myName,myPoint){
		students.push({
			name: myName,
			point: myPoint,
			show: function () { console.log('Студент %s набрал %s баллов', this.name, this.point); }
		});
	}
	
	studentsAndPoints.reduce( function(value, current, i) {
		if (i % 2 != 0){
			addStudent(value,current);
		}
		else {
			return current;
		}
	});

//Добавить в список студентов «Николай Фролов» и «Олег Боровой». У них пока по 0 быллов.	
var newStudents = ['Николай Фролов', 'Олег Боровой']; //Новые студенты
	newStudents.forEach( function(value) { addStudent(value,0); });


//Увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30, а Николаю Фролову на 10

/*Комментарий преподавателя 3. 
«Массив со списком студентов, для поиска нужного студента по индексу в массиве объектов-студентов» — проще использовать метод find чем indexOf и отдельный массив имен.*/
//**********ДОРАБОТКА
function findStudent(name, newpoint) {
	students.forEach( function(curStudent) {
		if (curStudent.name == name) {
			curStudent.point += newpoint;
		}
	});
}

var newPoints = ['Ирина Овчинникова', 30, 'Александр Малов', 30, 'Николай Фролов', 10];//Студенты которым надо увеличить баллы
	
	newPoints.reduce( function(value, current, i) {
		if (i % 2 != 0){
			findStudent(value, current);
		}
		else {
			return current;
		}
	});
	
	
//Вывести список студентов набравших 30 и более баллов без использования циклов
	function viewStudents(minPoint){ //Функция отображения студентов с количеством баллов больше входного
		students.forEach( function(student) {
			if (student.point >= minPoint) {
				student.show();
			}
		});
	}
	
viewStudents(30);

//Учитывая что каждая сделанная работа оценивается в 10 баллов, добавить всем студентам поле worksAmount равное кол-ву сделанных работ.
	var works = function () {
		console.log('Студент %s сдал работ:%s и набрал %s баллов', this.name, this.worksAmount, this.point);
	}
	students.forEach( function(student) {
		student.worksAmount = student.point / 10;
		student.echoWorks = works;
	});

//Дополнительное задание: добавить объекту students метод findByName, который принимает на вход имя студента и возвращает соответствующий объект, либо undefined.
function findByName(name){
	var person; //установка undefined
	students.forEach( function(curStudent) {
		if (curStudent.name == name) {
			person = curStudent;
			return person;//передача объекта
		}	
	});
	return person;//передача undefined
}
 
console.log (findByName('Александр Малов') );
console.log (findByName('Студент') );


/*
students.forEach( function(curStudent, i) {
		console.log( "студент " + curStudent.name + " набрал " +curStudent.point );
	});
*/
	